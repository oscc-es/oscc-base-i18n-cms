// Import plugins
const pluginRss = require('@11ty/eleventy-plugin-rss');
const socialSvgToPng = require('./src/plugins/social-svg-to-png.js');
const optimizeImg = require('./src/plugins/optimize-img-formats.js');
const minifyJs = require('./src/plugins/minify-js.js');
const {lang} = require('./src/_data/site.json');
const i18n = require('eleventy-plugin-i18n');
const translations = require('./src/_data/i18n');

// Import filters
const splitLines = require('./src/filters/split-lines.js');
const dateFilter = require('./src/filters/date-filter.js');
const w3DateFilter = require('./src/filters/w3-date-filter.js');
const langFilter = require('./src/filters/lang-filter.js');
const langLinksFilter = require('./src/filters/lang-links-filter.js');

// Import transforms
const htmlMinTransform = require('./src/transforms/html-min-transform.js');

// Import shortcodes
const imageShortcode = require('./src/shortcodes/image-shortcode.js');

module.exports = config => {
  // Tell 11ty to use the .eleventyignore and ignore our .gitignore file
  config.setUseGitIgnore(false);

  // Plugins
  config.addPlugin(pluginRss);
  config.addPlugin(socialSvgToPng);
  config.addPlugin(minifyJs);
  config.addPlugin(minifyJs, 'dist/');
  config.addPlugin(optimizeImg);
  config.addPlugin(i18n, {
    translations,
    fallbackLocales: {
      '*': lang
    }
  });

  // Filters
  config.addFilter('splitlines', splitLines);
  config.addFilter('dateFilter', dateFilter);
  config.addFilter('w3DateFilter', w3DateFilter);
  config.addFilter('langFilter', langFilter);
  config.addFilter('langLinksFilter', langLinksFilter);

  // Transforms
  config.addTransform('htmlmin', htmlMinTransform);

  // Shortcodes
  config.addNunjucksShortcode('img', imageShortcode);

  // Returns a collection of blog posts in reverse date order
  config.addCollection('blog', collection => {
    return [...collection.getFilteredByGlob('./src/posts/*/*.md')].reverse();
  });

  // Passthrough copy
  config.addPassthroughCopy({'src/favicon': '.'});
  config.addPassthroughCopy('src/js');
  config.addPassthroughCopy('src/*.js');
  config.addPassthroughCopy({'src/images/opt/': 'images/'});

  return {
    markdownTemplateEngine: 'njk',
    dataTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    dir: {
      input: 'src',
      output: 'dist'
    }
  };
};
