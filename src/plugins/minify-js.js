const fs = require('fs');
const {minify} = require('terser');

module.exports = function (
  eleventyConfig,
  dir = 'dist/js/components/',
  env = 'production'
) {
  if (process.env.NODE_ENV !== env) {
    return;
  }

  eleventyConfig.on('afterBuild', () => {
    if (!fs.existsSync(dir)) {
      throw new Error(`Js directory to minify does not exist: ${dir}`);
    }

    fs.readdir(dir, (err, files) => {
      if (files.length <= 0) {
        return;
      }

      files.forEach(async file => {
        if (!file.endsWith('.js')) {
          return;
        }

        const filePath = `${dir}${file}`;

        fs.writeFileSync(
          filePath,
          (await minify(fs.readFileSync(filePath, 'utf8'), {module: true})).code
        );
      });
    });
  });
};
