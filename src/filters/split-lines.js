// https://bnijenhuis.nl/notes/2021-05-10-automatically-generate-open-graph-images-in-eleventy/
module.exports = function splitLines(input, maxCharactersPerLine) {
  const parts = input.split(' ');

  return parts.reduce((prev, current) => {
    if (!prev.length) {
      return [current];
    }

    const lastCurrentLine = prev[prev.length - 1];
    if (lastCurrentLine.length + current.length > maxCharactersPerLine) {
      return [...prev, current];
    }

    prev[prev.length - 1] = lastCurrentLine + ' ' + current;
    return prev;
  }, []);
};
