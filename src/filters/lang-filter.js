module.exports = function langFilter(collection, lang) {
  return collection.filter(item =>
    item.inputPath !== undefined
      ? item.inputPath.endsWith(`/${lang}/${item.fileSlug}.md`)
      : false
  );
};
