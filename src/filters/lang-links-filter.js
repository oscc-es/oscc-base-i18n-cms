module.exports = function langLinksFilter(collection, lang, isTag = 0) {
  let langLinks = [];
  const tagCondition = (item, lang) => {
    return item.url == this.page.url.replace(lang, item.template._dataCache.lang);
  };
  const generalCondition = (item, lang) => {
    const itemLang = item.template._dataCache.lang;

    return (
      item.fileSlug == this.page.fileSlug ||
      (this.page.fileSlug == lang && item.fileSlug == itemLang)
    );
  };
  const condition = isTag ? tagCondition : generalCondition;

  for (let item of collection) {
    const itemLang = item.template._dataCache.lang;

    if (itemLang == lang) {
      continue;
    }

    if (condition(item, lang)) {
      langLinks.push({
        lang: itemLang,
        url: item.url,
        langName: item.data.langName
      });
    }
  }

  return langLinks;
};
