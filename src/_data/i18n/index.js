module.exports = {
  page_image_for: {
    en: 'Page image for',
    es: 'Imagen de página para'
  },
  home: {
    en: 'Home',
    es: 'Inicio'
  },
  navigation: {
    en: 'Main',
    es: 'Principal'
  },
  pagination: {
    en: 'Pagination',
    es: 'Paginación'
  },
  language: {
    en: 'Select language',
    es: 'Selecciona idioma'
  },
  language_selector: {
    en: 'Language selector',
    es: 'Selector de idioma'
  },
  skip_to_content: {
    en: 'Skip to content',
    es: 'Ir a contenido'
  },
  next: {
    en: 'Next',
    es: 'Siguiente'
  },
  previous: {
    en: 'Previous',
    es: 'Anterior'
  },
  about: {
    en: 'About',
    es: 'Acerca de'
  },
  blog: {
    en: 'Blog',
    es: 'Blog'
  }
};
