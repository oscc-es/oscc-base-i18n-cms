---
title: 'Offline'
layout: layouts/page.njk
permalink: /es/offline.html
eleventyExcludeFromCollections: true
---

Parece que no tiene conexión. Por favor, [vuelva a la página de inicio](/es/).
